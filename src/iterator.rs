use num_traits::Float;
use std::fmt::Debug;

use crate::Vec3;

impl<T: Debug + Float> IntoIterator for Vec3<T> {
    type Item = T;
    type IntoIter = std::array::IntoIter<Self::Item, 3>;

    fn into_iter(self) -> Self::IntoIter {
        std::array::IntoIter::new([self.x(), self.y(), self.z()])
    }
}

impl<'a, T: Debug + Float> IntoIterator for &'a Vec3<T> {
    type Item = T;
    type IntoIter = std::array::IntoIter<Self::Item, 3>;

    fn into_iter(self) -> Self::IntoIter {
        (*self).into_iter()
    }
}

impl<'a, T: Debug + Float> IntoIterator for &'a mut Vec3<T> {
    type Item = T;
    type IntoIter = std::array::IntoIter<Self::Item, 3>;

    fn into_iter(self) -> Self::IntoIter {
        (*self).into_iter()
    }
}

#[cfg(test)]
pub mod test {
    use crate::Vec3;

    #[test]
    fn into_iterator() {
        let sut = Vec3::new(1.0, 2.0, 3.0);

        let res = sut.into_iter().map(|x| x).collect::<Vec<f64>>();

        assert_eq!(res[0], 1.0);
        assert_eq!(res[1], 2.0);
        assert_eq!(res[2], 3.0)
    }
}
