#![allow(
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::non_ascii_literal,
    clippy::wildcard_imports,
    clippy::module_name_repetitions,
    incomplete_features
)]
#![warn(
    clippy::all,
    clippy::nursery,
    clippy::pedantic,
    clippy::cargo,
    clippy::unreadable_literal,
    rust_2018_idioms
)]
#![deny(
    clippy::pedantic,
    clippy::float_cmp_const,
    clippy::unwrap_used,
    clippy::future_not_send
)]
#![forbid(unsafe_code, bare_trait_objects)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

mod vec3;
pub use crate::vec3::Vec3;
