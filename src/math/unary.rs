use crate::Vec3;
use num_traits::Float;
use std::fmt::Debug;
use std::ops::Neg;

impl<T: Debug + Float> Neg for Vec3<T> {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Self(-self.0, -self.1, -self.2)
    }
}
