use num_traits::{Float, MulAdd};
use std::{
    fmt::Debug,
    ops::{Mul, Not},
};

use crate::Vec3;
impl<T: Debug + Float + Mul<Output = T> + MulAdd<Output = T>> Vec3<T> {
    // https://mathinsight.org/definition/magnitude_vector
    // sqrt(x^2 + y^2 + z^2)
    pub fn magnitude(&self) -> T {
        Float::sqrt(Float::mul_add(
            self.x(),
            self.x(),
            Float::mul_add(self.y(), self.y(), self.z().mul(self.z())),
        ))
    }

    pub fn is_null(&self) -> bool {
        self.magnitude().is_zero()
    }

    pub fn reflect(&self, n: &Self) -> Self {
        let x = (*n) * self.dot_product(n);
        (*self) - (x + x)
    }

    pub fn refract(&self, n: &Self, eta_i_over_eta_t: T) -> Self {
        let cos_theta = (-*self).dot_product(n).min(T::one());
        let r_out_perp = (*self + (*n * cos_theta)) * eta_i_over_eta_t;
        let r_out_parallel = *n
            * T::neg(T::sqrt(T::abs(
                T::one() - r_out_perp.magnitude().mul(r_out_perp.magnitude()),
            )));
        r_out_perp + r_out_parallel
    } // TODO test

    // https://mathworld.wolfram.com/UnitVector.html
    /// unit_vector returns vector of magnitude 1 with the same direction as self
    /// # Returns
    /// Some - Vector with magnitude of 1 that has the direction of `self`
    /// None - Indeterminate unit vector (Likely null vector)
    pub fn unit_vector(&self) -> Option<Self> {
        self.is_null().not().then(|| *self / self.magnitude())
    }

    // /// Returns a magnitude (almost) 1 unit vector with equal values
    // pub fn default_unit_vector() -> Self {
    //     let one_third_root = T::sqrt(T::one().div(T::from(3.0).unwrap()));
    //     Self::new(one_third_root, one_third_root, one_third_root)
    // }

    // https://en.wikipedia.org/wiki/Dot_product#Algebraic_definition
    // TODO: try using magnitude ||a||||b||cos(theta)
    pub fn dot_product(&self, rhs: &Self) -> T {
        Float::mul_add(
            self.x(),
            rhs.x(),
            Float::mul_add(
                self.y(),
                rhs.y(),
                Float::mul_add(self.z(), rhs.z(), T::zero()),
            ),
        )
    }

    // https://betterexplained.com/articles/cross-product/
    // (AyBz - AzBy, AzBx - AxBz, AxBy - AyBx)
    // TODO: try using magnitude ||a||||b||sin(theta)(unitvector)
    pub fn cross_product(&self, rhs: &Self) -> Self {
        Self(
            Float::mul_add(self.y(), rhs.z(), self.z().neg().mul(rhs.y())),
            Float::mul_add(self.z(), rhs.x(), self.x().neg().mul(rhs.z())),
            Float::mul_add(self.x(), rhs.y(), self.y().neg().mul(rhs.x())),
        )
    }
}
