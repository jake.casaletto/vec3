use std::fmt::Debug;
use std::ops::Mul;

use num_traits::Float;

use crate::Vec3;

impl<T: Debug + Float> Mul<Vec3<T>> for Vec3<T> {
    type Output = Self;

    fn mul(self, rhs: Vec3<T>) -> Self::Output {
        Self(self.0 * rhs.0, self.1 * rhs.1, self.2 * rhs.2)
    }
}

impl<T: Debug + Float> Mul<T> for Vec3<T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self::Output {
        Self(self.0 * rhs, self.1 * rhs, self.2 * rhs)
    }
}
