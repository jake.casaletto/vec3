pub mod arithmetic;
pub mod float_vector;
pub mod unary;

#[cfg(test)]
pub mod test {
    use crate::Vec3;

    pub mod arithmetic {
        use super::*;

        #[test]
        fn mul() {
            let lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = 3.0;

            let res = lhs * rhs;

            assert_eq!(res.x(), 3.0);
            assert_eq!(res.y(), 6.0);
            assert_eq!(res.z(), 9.0);
        }

        #[test]
        fn mul_vec3() {
            let lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = Vec3::new(1.0, 2.0, 3.0);

            let res = lhs * rhs;

            assert_eq!(res.x(), 1.0);
            assert_eq!(res.y(), 4.0);
            assert_eq!(res.z(), 9.0);
        }

        #[test]
        fn div() {
            let lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = 3.0;

            let res = lhs / rhs;

            assert_eq!(res.x(), 1.0 / rhs);
            assert_eq!(res.y(), 2.0 / rhs);
            assert_eq!(res.z(), 1.0);
        }

        #[test]
        fn mul_assign() {
            let mut lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = 3.0;

            lhs *= rhs;

            assert_eq!(lhs.x(), 3.0);
            assert_eq!(lhs.y(), 6.0);
            assert_eq!(lhs.z(), 9.0);
        }

        #[test]
        fn div_assign() {
            let mut lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = 3.0;

            lhs /= rhs;

            assert_eq!(lhs.x(), 1.0 / rhs);
            assert_eq!(lhs.y(), 2.0 / rhs);
            assert_eq!(lhs.z(), 1.0);
        }

        #[test]
        fn add() {
            let lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = Vec3::new(5.0, 5.0, 1.0);

            let res = lhs + rhs;

            assert_eq!(res.x(), 6.0);
            assert_eq!(res.y(), 7.0);
            assert_eq!(res.z(), 4.0);
        }

        #[test]
        fn sub() {
            let lhs = Vec3::new(6.0, 6.0, 8.0);
            let rhs = Vec3::new(2.0, 7.0, -2.0);

            let res = lhs - rhs;

            assert_eq!(res.x(), 4.0);
            assert_eq!(res.y(), -1.0);
            assert_eq!(res.z(), 10.0);
        }

        #[test]
        fn add_assign() {
            let mut sut = Vec3::new(1.0, 2.0, 3.0);
            let add_target = Vec3::new(5.0, 5.0, 1.0);

            sut += add_target;

            assert_eq!(sut.x(), 6.0);
            assert_eq!(sut.y(), 7.0);
            assert_eq!(sut.z(), 4.0);
        }

        #[test]
        fn sub_assign() {
            let mut sut = Vec3::new(6.0, 6.0, 8.0);
            let sub_target = Vec3::new(1.0, 7.0, -2.0);

            sut -= sub_target;

            assert_eq!(sut.x(), 5.0);
            assert_eq!(sut.y(), -1.0);
            assert_eq!(sut.z(), 10.0);
        }
    }

    pub mod operators {
        use super::*;

        #[test]
        fn neg() {
            let sut = Vec3::new(1.0, 2.0, 3.0);
            assert_eq!((-sut).magnitude(), Vec3::new(-1.0, -2.0, -3.0).magnitude());
        }
    }

    pub mod vector {
        use super::*;
        use num_traits::One;

        #[test]
        fn magnitude() {
            let sut = Vec3::new(1.0, 2.0, 3.0);

            let res = sut.magnitude();

            assert_eq!(res, 14.0_f64.sqrt());
        }

        #[test]
        fn dot_product() {
            let lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = Vec3::new(1.0, 2.0, 3.0);

            let res = lhs.dot_product(&rhs);

            assert_eq!(res, 14.0)
        }

        #[test]
        fn cross_product() {
            let lhs = Vec3::new(1.0, 2.0, 3.0);
            let rhs = Vec3::new(1.0, 2.0, 3.0);

            let res = lhs.cross_product(&rhs);

            assert_eq!(res, Vec3::default())
        }

        #[test]
        fn unit_vector() {
            let sut = Vec3::new(1.0, 2.0, 3.0);

            let res = sut.unit_vector().unwrap();

            assert!(res.magnitude().is_one());
            assert_eq!(res.x(), 1.0 / sut.magnitude());
            assert_eq!(res.y(), 2.0 / sut.magnitude());
            assert_eq!(res.z(), 3.0 / sut.magnitude());
        }

        #[test]
        fn reflect() {
            let sut = Vec3::new(1.0, 2.0, 3.0);
            let reflect_vector = Vec3::new(0.1, 0.1, 0.1);

            let res = sut.reflect(&reflect_vector);

            assert_eq!(res.x(), 0.88);
            assert_eq!(res.y(), 1.88);
            assert_eq!(res.z(), 2.88);
        }
    }

    pub mod null_vector {
        use super::*;

        #[test]
        fn unit_vector_is_indeterminate() {
            let sut = Vec3::<f64>::default().unit_vector();
            assert!(sut.is_none())
        }
    }
}
