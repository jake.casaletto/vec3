use derive_more::{AddAssign, Div, DivAssign, MulAssign, Sub, SubAssign};
use num_traits::Float;
use std::fmt::Debug;

#[derive(
    Default, Debug, Copy, Clone, PartialEq, Sub, AddAssign, SubAssign, Div, MulAssign, DivAssign,
)]
pub struct Vec3<T: Debug + Float>(T, T, T);

impl<T: Debug + Float> Vec3<T> {
    pub fn new(x: T, y: T, z: T) -> Self {
        Self(x, y, z)
    }
    pub fn x(&self) -> T {
        self.0
    }
    pub fn y(&self) -> T {
        self.1
    }
    pub fn z(&self) -> T {
        self.2
    }
}
